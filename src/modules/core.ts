import { EventBus } from "./eventBus";
import * as pm2 from "pm2";
import { ORM, Waterline } from "./orm";

interface TwitchHandler {
    id: string;
    handler: () => void;
}

export class Core {
    private static instance: Core;

    public _config: any = {};
    public _orm: any;
    public Waterline = Waterline;

    constructor() {
        EventBus.on("register", this.register.bind(this));
        EventBus.on("message", this.handleMessage.bind(this));
        process.on("beforeExit", this.gracefulQuit.bind(this));
        process.on("SIGINT", this.gracefulQuit.bind(this));
    }

    /**
     * Create Singleton instance of Core.
     */
    static getInstance() {
        if (!Core.instance) {
            Core.instance = new Core();
            Core.instance._orm = new ORM();

            const registration = {
                name: "CORE",
                target: "Core",
                model: {
                    config: {
                        datastore: "default",
                        primaryKey: "property",
                        attributes: {
                            property: {type: "string", required: true},
                            value: {type: "string"}
                        }
                    }
                }
            };

            EventBus.emit("register", registration);
        }
        
        return Core.instance;
    }

    /**
     * Handle Registration of a module that registers to this module.
     */
    register(module) {
        if (module.target === "Core") {
            if (module.model) {
                this._orm.registerModel(module.model);
            }
            console.info(`${module.name} just registered with CORE`);
        }
    }

    /**
     * Handle messages from a module.
     */
    handleMessage(message) {
        if (message === "modules loaded") {
            console.info(`All Modules Loaded`);
            this._orm.start().then(() => {
                EventBus.emit("message", "CORE started");
            });
        }
    }

    /**
     * Restarts the process if possable.
     */
    restart() {
        const core = Core.instance;

        pm2.list((err, pm2Processes) => {
            let restartProcess = undefined;

            pm2Processes.filter((pm2Process) => {
                if (err) console.error(err.stack || err);
                if (pm2Process.pid === process.pid) {
                    console.log(pm2Process.pid, process.pid);
                    restartProcess = pm2Process;
                }
            });

            if (restartProcess) {
                core.gracefulQuit(restartProcess);
            }
        });
    }

    /**
     * Gracefully quits the process.
     */
    gracefulQuit(pm2Process) {
        if (pm2Process) {
            pm2.restart(pm2Process, console.error);
        } else {
            process.exit(1);
        }
    }

    /**
     * Retrieve config setting from ORM.
     */
    getConfig(property) {

        return this._orm.getModel("config").findOne({
            where: {
                property: property
            }
        }).then(async (config) => {
            return config && config.value || undefined;
        });
    }

    /**
     * Set config setting to ORM.
     */
    setConfig(property, value) {
        return this._orm.getModel("config").findOrCreate({
            property: property
        }, {
            property: property,
            value: value
        }).exec(async (err, user, wasCreated) => {
            if (err) { return err; }

            if (!wasCreated) {
                return this._orm.getModel("config").updateOne({
                    property: property
                }).set({
                    property: property,
                    value: value
                });
            }
        });
    }
}
