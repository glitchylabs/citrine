import { Core } from "..";
import { EventBus } from "../eventBus";
import * as Discordjs from "discord.js";

export class Discord {
    private core = Core.getInstance();
    private options: Object = {};
    private client: Discordjs.Client = new Discordjs.Client(this.options);
    private commands = {
        help: {
            method: this.help.bind(this),
            description: "Calls this help screen..."
        }
    };
    private messagePrefix: string;

    constructor() {
        EventBus.on("register", this.register.bind(this));
        EventBus.on("message", this.handleMessage.bind(this));

        EventBus.emit("register", {
            name: "Discord",
            target: "Core",
        });
    }

    /**
     * Handle messages from a module.
     */
    handleMessage(message) {
        if (message === "ORM loaded") {
            /*
            * This is needed to set the data for the first time...I'll probably make a "Setup" module later.
            * this.core.setConfig("discord:clientToken", "1234567890"); - Get this token from Discord Developers page.
            * this.core.setConfig("discord:prefix", "!");  
            */
            this.core.getConfig("discord:clientToken").then((token) => {
                this.client.login(token);
            });

            this.client.on("ready", () => {
                console.log(`Logged in as ${this.client.user.username}!`);
                this.core.getConfig("discord:prefix").then((prefix) => {
                    this.messagePrefix = prefix;
                });

                this.client.on("message", this.discordMessage.bind(this));
            });

            EventBus.emit("register", {
                name: "Discord",
                target: "Echo",
            });
            EventBus.emit("register", {
                name: "Discord",
                target: "SystemInfo",
            });
        }
    }

    discordMessage(msg) {
        if (msg.content.startsWith(this.messagePrefix) && msg.author.id != this.client.user.id) {
            const message = msg.content.replace(this.messagePrefix, "");
            const guild = msg.guild || undefined;
            switch (msg.channel.type) {
                case "dm":
                    break;
                case "text":
                default:
                    if (this.commands.hasOwnProperty(message.split(" ")[0])) {
                        this.commands[message.split(" ")[0]].method(message, guild).then((response) => {
                            switch (response.type) {
                                case "table":
                                    const embed = new Discordjs.RichEmbed();
                                    const columns = [];

                                    embed.setColor("0xeb9b07");
                                    embed.setTitle(response.content.title);

                                    response.content.headers.forEach((header, index) => {
                                        columns[index] = [header, new Array(response.content.rows.length)];
                                    });
                                    response.content.rows.forEach((row) => {
                                        row.forEach((data, index) => {
                                            columns[index][1].push(data);
                                        });
                                    });
                                    
                                    columns.forEach((column) => {
                                        embed.addField(column[0], column[1].join("\n"), true);
                                    });

                                    msg.channel.send(embed);
                                    break;
                                case "text":
                                    msg.channel.send(`${response.content}`);
                                    break;
                                default:
                                    break;
                            }
                        });
                    }
                    break;
            }
        }
    }

    register(module) {
        if (module.target == "Discord") {
            this.commands = {...this.commands, ...module.commands};
        }
    }

    help() {
        const helpfile = {
            type: "table",
            content: {
                title: "Let's see how i can help:",
                headers: ["Command", "Description"],
                rows: []
            }
        };
        Object.keys(this.commands).forEach((command) => {
            helpfile.content.rows.push([command, this.commands[command].description]);
        });
        
        return new Promise((resolve) => {
            resolve(helpfile);
        });
    }
}