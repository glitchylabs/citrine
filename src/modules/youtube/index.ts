import * as ytdl from "ytdl-core";
import * as ytsr from "ytsr";
import { EventBus } from "../eventBus";

export class Youtube {
    private registration = {};
    private commands = {
        play: this.youtube.bind(this)
    };

    constructor() {
        EventBus.on("register", this.register.bind(this));

        this.registration = {
            name: "Youtube",
            target: "Core",
            commands: this.commands
        };
        EventBus.emit("register", this.registration);
    }

    /**
     * searched for youtube by url, video id, then text
     */
    youtube(data: string) {
        const args = data.split(" ");
        // This kinda looks like a URL
        if (ytdl.validateURL(args[1])) {
            return this.lookupVideo(args[1]).then((info) => {
                return info.title;
            });
        } 
        // Lets look up by ID
        else if (ytdl.validateID(args[1])) {
            return this.lookupVideo(args[1]).then((info) => {
                return info.title;
            });
        }
        // That didn't work, searching text
        else {
            return this.searchVideo(args.slice(1).join(" ")).then((info) => {
                return info.items[0].title;
            });
        }
    }
    
    lookupVideo(searchString) {
        return ytdl.getInfo(searchString);
    }

    searchVideo(searchString) {
        return ytsr.getFilters(searchString).then((filters) => {
            const filter = filters.get("Type").find(o => o.name === "Video");
            return ytsr(undefined, {limit: 1, nextpageRef: filter.ref}).then((searchResults) => {
                return searchResults;
            });
        });
    }

    register(module) {
        if (module.target == "Youtube") {
            EventBus.emit("register", {
                name: "Youtube",
                target: module.name,
                commands: this.commands
            });
        }
    }
}