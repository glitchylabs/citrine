import { EventBus } from "../eventBus";
import * as si from "systeminformation";

export class SystemInfo {
  private registration = {};
  private commands = {
    info: {
      method: this.info.bind(this),
      description: "Sends Info to the Clients"
    }
  };

  constructor() {
    EventBus.on("register", this.register.bind(this));

    this.registration = {
      name: "SystemInfo",
      target: "Core",
      commands: this.commands
    };
    EventBus.emit("register", this.registration);
  }

  info() {
    return new Promise((resolve, reject) => {
      si.system((info) => {
        resolve({
          type: "text",
          content: `${info.manufacturer} - ${info.model}`
        });
      });
    });
  }
  
  register(module) {
    if (module.target == "SystemInfo") {
      EventBus.emit("register", {
        name: "SystemInfo",
        target: module.name,
        commands: this.commands
      });
    }
  }
}