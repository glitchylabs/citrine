import { Core } from "..";
import { EventBus } from "../eventBus";
import { Client, ClientMode, Message } from "irc";

export class Irc {
    private core = Core.getInstance();
    private client: Client;
    private options: {
        mode?: ClientMode;
        server?: string;
        port?: string;
        nickname?: string;
    };
    private commands: {};
    private messagePrefix: string;

    constructor() {
        EventBus.on("register", this.register.bind(this));
        EventBus.on("message", this.handleMessage.bind(this));

        EventBus.emit("register", {
            name: "Irc",
            target: "Core",
            commands: {
            }
        });

        this.options = {};
    }

    /**
     * Handle messages from a module.
     */
    handleMessage(message) {
        if (message === "ORM loaded") {
            /*
            * This is needed to set the data for the first time...I'll probably make a "Setup" module later.
            * this.core.setConfig("irc:clientToken", "1234567890"); - Get this token from Discord Developers page.
            * this.core.setConfig("irc:prefix", "!");  
            */
            this.core.getConfig("irc:prefix").then((prefix) => {
                this.messagePrefix = prefix;
            });
            this.core.setConfig("irc:mode", "tls");
            this.core.setConfig("irc:server", "irc.tas.bot");
            this.core.setConfig("irc:port", "6667");
            this.core.setConfig("irc:nickname", "Citrine");
            // this.core.getConfig("irc:").then(() => {  
            // });
            const promises = [
                this.core.getConfig("irc:mode").then((x) => this.options.mode = x),
                this.core.getConfig("irc:server").then((x) => this.options.server = x),
                this.core.getConfig("irc:port").then((x) => this.options.port = x),
                this.core.getConfig("irc:nickname").then((x) => this.options.nickname = x)
            ];

            Promise.all(promises).then(() => {
                this.client = new Client(this.options.mode, this.options.server, this.options.port, this.options.nickname);
                this.client.connect();
                this.client.joinChannel("#opers");

                this.client.on("message", (msg: Message) => {
                    console.log(msg.command, msg.raw);
                    switch (msg.command.toUpperCase()) {
                        case "422":
                        case "376":
                            console.log(`Logged in as ${this.client.nickname}!`);
                        break;
                        case "PRIVMSG":
                            this.ircMessage(msg);
                        break;
                        default:
                            // console.log(`WTF? some message i don't know about: ${msg.command}, ${msg.raw}`);
                        break;
                    }
    
                });
            });

            EventBus.emit("register", {
                name: "Irc",
                target: "Echo",
            });
            
            EventBus.emit("register", {
                name: "Irc",
                target: "Youtube",
            });
        }
    }

    ircMessage(msg: Message) {
        const channel = (msg.channel.startsWith("#")) ? msg.channel : msg.sender;
        if (msg.message.startsWith(this.messagePrefix)) {
            const message = msg.message.replace(this.messagePrefix, "");
            if (this.commands.hasOwnProperty(message.split(" ")[0])) {
                this.commands[message.split(" ")[0]](message).then((response) => {
                    this.client.sendData(`PRIVMSG ${channel} :${response}`, false);
                });
            }
        }
    }

    register(module) {
        if (module.target == "Irc") {
            this.commands = {...this.commands, ...module.commands};
        }
    }

    getClient() {
        return this.client;
    }
}