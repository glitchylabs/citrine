import { EventBus } from "../eventBus";

export class Echo {
    private registration = {};
    private commands = {
        echo: {
            method: this.echo.bind(this),
            description: "Echos out whatever you pass in as arguments"
        }
    };

    constructor() {
        EventBus.on("register", this.register.bind(this));

        this.registration = {
            name: "Echo",
            target: "Core",
            commands: this.commands
        };
        EventBus.emit("register", this.registration);
    }

    /**
     * Returns what you put in
     */
    echo(data) {
        return new Promise((resolve, reject) => {
            if (data.indexOf(" ") > -1) {
                resolve({
                    type: "text",
                    content: data.slice(data.indexOf(" ") + 1)
                });
            } else {
                reject();
            }
        });
    }
    
    register(module) {
        if (module.target == "Echo") {
            EventBus.emit("register", {
                name: "Echo",
                target: module.name,
                commands: this.commands
            });
        }
    }
}