import * as EventEmitter from "events";

export const EventBus = new EventEmitter();

if (process.env.ENVIRONMENT === "development") {
    EventBus.on("register", (msg) => {
        handleMessage("register", msg);
    });
    EventBus.on("message", (msg) => {
        handleMessage("message", msg);
    });
}

function handleMessage(type, msg) {
    console.log(`\x1b[33m[${new Date().toTimeString().slice(0, 8)}]\x1b[0m ${type}: ${msg}`);
}
