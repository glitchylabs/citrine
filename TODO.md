Framework:
- Update README - Really need to know hwo this works and not some BS readme.
- Update Framework to register via capabilities and not module names
- Update Registration to include "help"

Modules:
- Add Audio processing to youtube module
- Add Audio pass-through to discord module
- Add Twitch module (maybe extend IRC)
- Add Queue module
- Add Queue-Admin module (This is for streaming and allows moderators to approve/decline playlist items.)
- Add Radio module
- Add Permissions module (Discord and IRC can connect to this for permissions)

Bugs:
- Fix empty play command bug